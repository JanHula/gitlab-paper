from collections import Counter, defaultdict, OrderedDict
from typing import Counter as CounterType, Dict, List

import numpy
from labours import ProtobufReader


class OwnershipProtobufReader(ProtobufReader):
    """Protobuf reader for Couples analysis - gives list of of files per developer. """

    def get_file_total_lines(self):
        repo_name = self.get_name().split("/")[-1].split("_")[0]
        file_index = ["gitlab.com/gitlab-org/%s.git//%s" % (repo_name, file_name)
                      for file_name in self.contents["Couples"].file_couples.index]
        file_total_lines = {}
        for file_path, entry in zip(file_index, self.contents["Burndown"].files_ownership):
            file_total_lines[file_path] = sum(entry.value.values())
        return file_total_lines
        
    def get_files_touched_per_dev(self) -> Dict[str, CounterType[str]]:
        repo_name = self.get_name().split("/")[-1].split("_")[0]
        authors = self.contents["Couples"].people_couples.index
        file_index = ["gitlab.com/gitlab-org/%s.git//%s" % (repo_name, file_name)
                      for file_name in self.contents["Couples"].file_couples.index]
        result = {}
        for file_path, entry in zip(file_index, self.contents["Burndown"].files_ownership):
            for author_index, n_lines in entry.value.items():
                author = authors[author_index]
                if author not in result:
                    result[author] = Counter()
                result[author][file_path] += n_lines
        return result


def update_identity_index(identity: str, identity_index: Dict):
    """
    Update identity index + merge identities if needed.

    :param identity: developer identity like "name | email"
    :param identity_index: mapping of names/emails to unique developer index
    """
    value = None
    identities = identity.split("|")
    for ident in identities:
        if ident in identity_index:
            if value is not None and value != identity_index[ident]:
                merge_identities(identity_index=identity_index, index1=value,
                                 index2=identity_index[ident])
            value = identity_index[ident]
    if value is None:
        value = len(identity_index)
    for ident in identities:
        identity_index[ident] = value


def get_identity_index(identity: str, identity_index: Dict) -> int:
    """
    Get unique identity index of developer.

    :param identity: developer identity like "name | email"
    :param identity_index: mapping of names/emails to unique developer index
    :return: unique developer index
    """
    indices = []
    for identity in identity.split("|"):
        try:
            indices.append(identity_index[identity])
        except KeyError:
            continue
    assert len(
        set(indices)) == 1, f"Broken identity matching, number of indices {len(set(indices))} != 1"
    return indices[0]


def merge_identities(identity_index: Dict, index1: int, index2: int):
    """
    Merge two developers into one (happens when you merge identities for developer from multiple
    repositories) and update `identity_index`.

    :param identity_index: mapping of names/emails to unique developer index
    :param index1: index of developer
    :param index2: index of developer
    :return:
    """
    identities = [k for k, v in identity_index.items() if v in (index1, index2)]
    new_index = min(index1, index2)
    for i in identities:
        identity_index[i] = new_index


def collect_unique_identities(statistics_loc: List) -> (Dict, Dict):
    """
    Collect unique developers identities.

    :param statistics_loc: list of locations for statistic per repository
    :return:  identity_index, index_identities
              identity_index - mapping {Name/email/etc: index}.
                               Several names & emails may have the same index
              index_identities - mapping {index: list of Names/emails/etc}
    """
    identity_index = {}
    reader = ProtobufReader()

    for repo_stat_loc in statistics_loc:
        # read statistics
        try:
            reader.read(str(repo_stat_loc))
        except ValueError:
            print(f"{repo_stat_loc} wasn't extracted successfully")
            pass
        # aggregate contribution
        people, days = reader.get_devs()
        for _, day in sorted(days.items()):
            for developer_identity, stats in day.items():
                #print("dev identity :", developer_identity)
                # check & update identities_index
                developer_identity = people[developer_identity]
                update_identity_index(identity=developer_identity,
                                       identity_index=identity_index)

    # postprocess because after merging there could be missed values
    index_identities = defaultdict(list)
    index_new_index = {k: i for i, k in enumerate(sorted(set(identity_index.values())))}
    for identity in identity_index:
        identity_index[identity] = index_new_index[identity_index[identity]]
        index_identities[identity_index[identity]].append(identity)
    print(f"Number of unique developers: {len(set(list(identity_index.values())))}")

    return identity_index, index_identities


def collect_unique_identities_ownership(statistics_loc: List) -> (Dict, Dict):
    """
    Collect unique developers identities, for ownership analysis.

    :param statistics_loc: list of locations for statistic per repository
    :return:  identity_index, index_identities
              identity_index - mapping {Name/email/etc: index}.
                               Several names & emails may have the same index
              index_identities - mapping {index: list of Names/emails/etc}
    """
    identity_index = {}
    reader = OwnershipProtobufReader()

    for repo_stat_loc in statistics_loc:
        # read statistics
        try:
            reader.read(str(repo_stat_loc))
        except ValueError:
            print(f"{repo_stat_loc} wasn't extracted successfully")
            pass
        authors = reader.contents["Couples"].people_couples.index
        for entry in reader.contents["Burndown"].files_ownership:
            for author_index in entry.value:
                author = authors[author_index]
                update_identity_index(identity=author,
                                       identity_index=identity_index)

    # postprocess because after merging there could be missed values
    index_identities = defaultdict(list)
    index_new_index = {k: i for i, k in enumerate(sorted(set(identity_index.values())))}
    for identity in identity_index:
        identity_index[identity] = index_new_index[identity_index[identity]]
        index_identities[identity_index[identity]].append(identity)
    print(f"Number of unique identities: {len(set(list(identity_index.values())))}")

    return identity_index, index_identities


def collect_files_total_lines(statistics_loc: List) -> (Dict[str, int]):
    """
    Collect total lines for each file.

    :param statistics_loc: list of locations for statistic per repository
    :return: mapping {filepath: n_total_lines}.
    """
    files_total_lines = {}
    reader = OwnershipProtobufReader()

    for repo_stat_loc in statistics_loc:
        # read statistics
        try:
            reader.read(str(repo_stat_loc))
        except ValueError:
            print(f"{repo_stat_loc} wasn't extracted successfully")
            pass
        files_total_lines.update(reader.get_file_total_lines())
    return files_total_lines


def aggregate_contribution_per_developer(statistics_loc: List, identity_index: Dict) -> Dict:
    """
    Aggregate contribution per developer.

    :param statistics_loc: list of locations for statistic per repository
    :param identity_index: mapping {Name/email/etc: index}. Several names & emails may have the
                           same index
    :return: mapping {DeveloperIndex: DevDay}
    """
    # aggregate dayly statistics per developer
    aggregated_dev = OrderedDict()

    reader = ProtobufReader()
    """
    # Format:
    {day: {
        developer_id: DevDay(Commits=n_commits, Added=n_lines_added, 
                             Removed=n_lines_removed, Changed=n_lines_changes, 
                             Languages={language: [added_lines, removed_lines, changed_lines]}
     }
    }
    # it shows changes that was done by developer K at day D - number of commits, changes per lang
    """

    for repo_stat_loc in statistics_loc:
        # read statistics
        try:
            reader.read(str(repo_stat_loc))
        except ValueError:
            print(f"{repo_stat_loc[68:]} wasn't extracted successfully")
            continue
        # aggregate contribution
        people, days = reader.get_devs()
        for _, day in sorted(days.items()):
            for developer_identity, stats in day.items():
                # check & update identities_index
                developer_identity = people[developer_identity]
                dev_index = get_identity_index(identity=developer_identity,
                                               identity_index=identity_index)
                if dev_index in aggregated_dev:
                    aggregated_dev[dev_index] = aggregated_dev[dev_index].add(stats)
                else:
                    aggregated_dev[dev_index] = stats
    return aggregated_dev


def collect_files_per_developer(statistics_loc: List, identity_index: Dict) -> Dict:
    """
    Files that were touched by developer.

    :param statistics_loc: list of locations for statistic per repository
    :param identity_index: mapping {Name/email/etc: index}. Several names & emails may have the
                           same index
    :return: mapping {DeveloperIndex: list of files (filename includes repository)}
    """

    # files that were touched by developer
    dev_files = defaultdict(list)
    for repo_stat_loc in statistics_loc:
        dev_file_reader = OwnershipProtobufReader()
        # read statistics
        try:
            dev_file_reader.read(str(repo_stat_loc))
        except ValueError:
            print(f"'{repo_stat_loc}' wasn't extracted successfully")
            continue
        # files per developer
        for developer_identity, files in dev_file_reader.get_files_touched_per_dev().items():
            try:
                dev_index = get_identity_index(identity=developer_identity,
                                               identity_index=identity_index)
            except AssertionError:
                print(f"'{developer_identity}' probably modified only binary files or merged "
                      f"commits")
                continue
            dev_files[dev_index].extend(files)

    #print(f"Number of developers: {len(dev_files)}")
    return dev_files


def get_dev_topics_mat(file_topics_npz, file_index, file_topics_mat, dev_files):
    """
    Prepare matrix (n_developers, n_topics) based on file topics & list of files that were touched
    by developer.

    :param file_index: mapping {filename: row index}.
    :param file_topics_mat: matrix with size (n_files, n_topics).
    :param dev_files: files that were touched by developer.
    :return:
    """
    # found = set()
    # not_found = set()
    # users = []
    # empty_users = 0
    # with numpy.load(file_topics_npz) as file_topics:
    #     file_index = {filename: i for i, filename in enumerate(file_topics.files)}
    #     file_topics_arr = numpy.vstack([file_topics[filename] for filename in file_topics.files])
    #     for index, files in tqdm(sorted(files_list.items(), key=lambda x: x[0]),
    #                              desc="Identities"):
    #         selected_rows = [file_index[filename] for filename in files if filename in file_index]
    #         if not selected_rows:
    #             users.append(numpy.zeros((128,), dtype=numpy.float))
    #             empty_users += 1
    #         else:
    #             users.append(file_topics_arr[selected_rows].mean(axis=0))
    # users_arr = numpy.vstack(users)
    # print("files found: %d, files not found: %d" % (len(found), len(not_found)))
    # print("empty users: %d, non-empty users: %d" % (empty_users, len(files_list) - empty_users))
    # print(users_arr.shape)


def get_topics_per_dev(file_index, file_topics_mat, dev_files):
    """
    Compute topics per developer based on list of files that were touched by developer.

    :param file_index: mapping {filename: row index}.
    :param file_topics_mat: matrix with size (n_files, n_topics).
    :param dev_files: files that were touched by developer.
    :return:
    """
    selected_rows = [file_index[filename] for filename in dev_files if filename in file_index]
    if not selected_rows:
        return numpy.zeros((128,), dtype=numpy.float)
    else:
        return file_topics_mat[selected_rows].mean(axis=0)
