FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y --no-install-suggests --no-install-recommends locales ca-certificates python3 python3-dev gcc g++ wget git && \
    locale-gen en_US.UTF-8 && \
    (wget -O - https://bootstrap.pypa.io/get-pip.py || wget -O - https://raw.githubusercontent.com/pypa/get-pip/master/get-pip.py) | python3 && \
    pip3 install --no-cache labours umap-learn pandas natural scikit-learn seaborn tqdm pyldavis \
                            jupyter jupyter_contrib_nbextensions jupyter_nbextensions_configurator && \
    apt-get remove -y python3-dev gcc g++ && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    jupyter contrib nbextension install && \
    jupyter nbextensions_configurator enable && \
    echo '{\
  "load_extensions": {\
    "nbextensions_configurator/config_menu/main": true,\
    "execute_time/ExecuteTime": true,\
    "notify/notify": true\
  },\
  "ExecuteTime": {\
    "display_in_utc": true\
  }\
}' > /root/.jupyter/nbconfig/notebook.json && \
    echo '#!/bin/bash\n\
\n\
echo\n\
echo "	$@"\n\
echo\n\' > /usr/local/bin/browser && \
    chmod +x /usr/local/bin/browser

RUN wget -O - https://github.com/src-d/hercules/releases/download/v10.0.0/hercules.linux_amd64.gz | gzip -d > /usr/local/bin/hercules && \
    chmod +x /usr/local/bin/hercules

COPY . /root
EXPOSE 8888
ENV BROWSER /usr/local/bin/browser
ENV LC_ALL en_US.UTF-8
WORKDIR /root
CMD ["jupyter", "notebook", "--ip" , "0.0.0.0", "--port", "8888", "--allow-root"]
